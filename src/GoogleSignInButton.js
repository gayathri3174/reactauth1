// GoogleSignInButton.js
import React from 'react';
import { signInWithPopup } from 'firebase/auth';
import { provider, auth } from './firebase';

const GoogleSignInButton = () => {
  const handleGoogleSignIn = async () => {
    try {
      const result = await signInWithPopup(auth, provider);
      const user = result.user;
      console.log(user);
    } catch (error) {
      console.error(error.code, error.message);
    }
  };

  return (
    <button onClick={handleGoogleSignIn}>
      Sign in with Google
    </button>
  );
};

export default GoogleSignInButton;
